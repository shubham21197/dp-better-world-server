package au.com.digitalpurpose.betterworld.server.api;

import java.util.Collection;

import au.com.digitalpurpose.betterworld.server.model.Goal;
import au.com.digitalpurpose.betterworld.server.service.GoalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/goal")
public class GoalApi {

    private final GoalService goalService;

    @Autowired
    public GoalApi(GoalService goalService) {
        this.goalService = goalService;
    }

    @GetMapping
    public Collection<Goal> listGoals() {
        return goalService.listGoals();
    }
}
