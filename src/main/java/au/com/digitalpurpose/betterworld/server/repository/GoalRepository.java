package au.com.digitalpurpose.betterworld.server.repository;

import au.com.digitalpurpose.betterworld.server.model.Goal;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GoalRepository  extends JpaRepository<Goal, Long> {
}
