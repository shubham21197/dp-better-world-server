package au.com.digitalpurpose.betterworld.server.request.idea;

public class CreateIdeaRequest {

    private String title;
    private String submitterName;
    private String description;
    private String imageUrl;
    private String goal;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubmitterName() {
        return submitterName;
    }

    public void setSubmitterName(String submitterName) {
        this.submitterName = submitterName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGoal() { return goal; }

    public void setGoal (String goal) { this.goal = goal; }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
