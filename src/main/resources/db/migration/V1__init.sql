create table goal
(
    id    INT AUTO_INCREMENT PRIMARY KEY,
    key   VARCHAR NOT NULL,
    title VARCHAR NOT NULL
);

insert into goal (key, title)
values ('poverty', 'No Poverty');
insert into goal (key, title)
values ('hunger', 'Zero Hunger');
insert into goal (key, title)
values ('health', 'Good Health and Well-being');
insert into goal (key, title)
values ('education', 'Quality Education');
insert into goal (key, title)
values ('genderEquality', 'Gender Equality');
insert into goal (key, title)
values ('sanitation', 'Clean Water and Sanitation');
insert into goal (key, title)
values ('energy', 'Affordable and Clean Energy');
insert into goal (key, title)
values ('economy', 'Decent Work and Economic Growth');
insert into goal (key, title)
values ('industry, Innovation and Infrastructure', 'Industry, Innovation and Infrastructure');
insert into goal (key, title)
values ('inequality', 'Reduced Inequality');
insert into goal (key, title)
values ('sustainability', 'Sustainable Cities and Communities');
insert into goal (key, title)
values ('consumptionProduction', 'Responsible Consumption and Production');
insert into goal (key, title)
values ('climateAction', 'Climate Action');
insert into goal (key, title)
values ('belowWater', 'Life Below Water');
insert into goal (key, title)
values ('onLand', 'Life on Land');
insert into goal (key, title)
values ('peace', 'Peace and Justice Strong Institutions');
insert into goal (key, title)
values ('partnerships', 'Partnerships to achieve the Goal');

CREATE TABLE idea
(
    id             INT AUTO_INCREMENT PRIMARY KEY,
    goal           VARCHAR,
    title          VARCHAR NOT NULL,
    description    VARCHAR,
    thumbnail_url  VARCHAR,
    submitter_name VARCHAR
);

insert into idea (goal, title, description, thumbnail_url)
values ('sustainability', 'Clothing made from recycled material',
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        'https://scx2.b-cdn.net/gfx/news/hires/2018/sustainable.jpg');

insert into idea (goal, title, description, thumbnail_url)
values ('education', 'Free, online school for everyone',
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        'https://cdn-res.keymedia.com/cms/images/ca/126/0348_637326621445860317.jpg');

insert into idea (goal, title, description, thumbnail_url)
values ('climateAction', 'Insect farming as a food source',
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        'https://i.pinimg.com/564x/95/05/d7/9505d74989aa7c2fc4b92674019fc080.jpg');

insert into idea (goal, title, description, thumbnail_url)
values ('climateAction', 'Ghost net removal program',
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        'https://www.diveagainstdebris.org/sites/www.diveagainstdebris.org/files/styles/post/public/Scuba%20Fish%20Thailand%20%28C%29%20Liquid%20Lense.jpg');

insert into idea (goal, title, description, thumbnail_url)
values ('climateAction', 'Justice reinvestment community resilience program',
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        'https://assets.change.org/photos/8/cj/tz/SAcjTzoknSiBqwy-400x400-noPad.jpg?1515940789');
